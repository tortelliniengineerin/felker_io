/*
 * felker_io_uart.h
 *
 *  Created on: Mar 8, 2016
 *      Author: Nick Felker
 */

#ifndef FELKER_UART_H
#define FELKER_UART_H

typedef struct {
	void (*onReceived) (char);
} uartCallback;

/* Let's declare our uart function pointers */
uartCallback uartCallback1;

/**
 * Inits a uart pin with a 9600 baud rate
 * @param uart A pointer to the given uartPin struct you want to initialize
 * @param onreceive The function that would be called when you receive data
 */
void initUart(uartPin *uart, void (*onreceive)(char));
/**
 * Sends a character to a UART buffer to be transfered
 * @param uart The uartPin struct that data will be written to
 * @param c The character that will be transferred
 */
void uartTransfer(uartPin *uart, char c);
/**
 * Sends a character to a UART buffer to be transfered
 * @param uart The uartPin struct that data will be written to
 * @param c The character that will be transferred
 */
void uartUnsignedTransfer(uartPin *uart, unsigned char c);
/**
 * Checks if the UART buffer is ready to receive more characters
 */
bool uartBufferReady(int uartPin);

#ifdef USE_FELKER_UART

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_A1_VECTOR))) USCI_A1_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(UCA1IV,4))
  {
  case 0:break;                             // Vector 0 - no interrupt
  case 2:                                   // Vector 2 - RXIFG
	  while (uartBufferReady(1) == false);             // USCI_A0 TX buffer ready?
	  uartCallback1.onReceived(UCA1RXBUF);
    break;
  case 4:break;                             // Vector 4 - TXIFG
  default: break;
  }
}
#endif

#endif


