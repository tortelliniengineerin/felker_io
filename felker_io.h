/*
 * felker_io.h
 *
 *  Created on: Mar 8, 2016
 *      Author: Nick Felker
 */
#include <msp430f5529.h>

#ifndef FELKER_IO_H
#define FELKER_IO_H


typedef enum { false, true } bool;
typedef enum { LOW = 0, HIGH = 1} digitalio;
typedef struct {
	unsigned int uart_id;
	unsigned int port_id;
	unsigned int tx;
	unsigned int rx;
} uartPin;
/**
 * The Microcontroller struct makes it easy to get pins and pin values
 */
typedef struct {
	volatile unsigned int p1;
	volatile unsigned int p2;
	volatile unsigned int p3;
	volatile unsigned int p4;
	volatile unsigned int p5;
	volatile unsigned int p6;

	unsigned int b0;
	unsigned int b1;
	unsigned int b2;
	unsigned int b3;
	unsigned int b4;
	unsigned int b5;
	unsigned int b6;
	unsigned int b7;

	uartPin uart0;
	uartPin uart1;
} microcontroller;

/**
 * States that the pin is an output
 */
#define OUTPUT 1
/**
 * States that the pin is an input
 */
#define INPUT 2
/**
 * States that the pin is not for general input or output
 */
#define PERIPHERAL 3
/**
 * States that the pin is an input and has a pullup resistor
 */
#define INPUT_PULLUP 4

/**
 * Sets the given pin direction and its selection value
 * @param port The port value, P1 - P4
 * @param bit The pin number 0-7
 * @param mode The type of pin mode, ie. INPUT or OUTPUT
 */
void pinMode(volatile unsigned int port, unsigned int bit, unsigned int mode);
/**
 * Changes the value of the pin from HIGH to LOW or vice versa if it is an output pin.
 * @param port The port value, P1 - P4
 * @param bit The pin number 0-7
 */
void togglePinValue(volatile unsigned int port, unsigned int bit);
/**
 * Writes a binary value to a specific pin
 */
void digitalWrite(volatile unsigned int port, unsigned int bit, unsigned char val);
/**
 * Retrieves the binary value that is read on a specific pin
 */
digitalio digitalRead(volatile unsigned int port, unsigned int bit);
/**
 * Stops the watchdog timer. Is called in the superMain function
 */
void stopWatchdogTimer();
/**
 * Sends a no operation command, usually for debug purposes
 */
void nopIt();
/**
 * Enters low power mode. Normal code will not run, but interrupts may work
 */
void enterLowPowerMode();
/**
 * This should be the first function called. It will call initialization functions.
 * @return A GPIO object which you can use to do pin inits
 */
microcontroller getMicroController();

#endif
