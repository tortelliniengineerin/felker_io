# Felker IO
Felker IO is a library for embedded systems that wraps a lot of the embedded system functions to make it much more user-friendly, making it like an Arduino or C++ type of syntax.

## Initialization
To begin, you must:

    #include "felker.h"
    
And link to the `felker.c` file. This will let you access the base classes.

## Initiailize in `Main`
In your `main`, you need to grab a `microcontroller` object by calling `getMicrocontroller`. This will give you access to the pins for reading and writing.

## Pin I/O
To start interfacing with a pin, you need the microcontroller to know which kind of pin it is. You can initialize a pin direction by calling:

    pinMode(port, bit, mode)
    
If you want to set the LED, P1.0, to be an output:

    pinMode(msp.p1, msp.b0, OUTPUT)
    
There are several types of modes:

* OUTPUT - The pin is outputting voltage
* INPUT - The pin is inputting voltage
* PERIPHERAL - The pin is not for general input or output
* INPUT_PULLUP - The pin is an input and has a pullup resistor (like for push buttons)

### Writing to a Pin
To write a value to an output pin, call:

    digitalWrite(msp.p1, msp.b0, HIGH);
    
This uses the `digitalio` type, although it can just be an integer.

### Reading from a Pin
To read from a pin:
    
    digitalRead(msp.p1, msp.b0)
    
This returns a `digitalio` type, which you can use for conditionals:

    if(digitalRead(msp.p1, msp.b0) == HIGH)
        //The input is high
        
### Toggle Pin
Sometimes you want to switch the value without caring if it is true or false:

    togglePinValue(msp.p1, msp.b0);

## Other Utility Functions
### Do Nothing

    nopIt();
    
### Enter Low Power Mode
    
    enterLowPowerMode();
    
## UART
To use the UART library, `#include felker_io_uart.h`.

### Initialization
To initialize a specific `uartPin` type, call `initUart` and pass as parameters the `uartPin` and a `uartCallback` function pointer which will be called when a character is received.

To initialize uart's 2nd pin (UART 1), call:

    void flipLED(char rx) {
        togglePinValue(msp.p1, msp.b0);
    }
    void main() {
        initUart(&(msp.uart1), &flipLED); 
    }   
    
### Send a char
To send a single character in a blocking command, call:

    `uartTransfer(&(msp.uart1), 'a');

## Included objects
There are a few classes that are built-in to the library.

### `bool`
C does not have a direct boolean type. This class gives you access to using a `bool` as an enum. The `bool` has values of `false` and `true`.

### `digitalio`
A pin can read `LOW` or `HIGH`. Although these are aliases for 0 and 1 respectively, the `digitalio` type makes it easy for the developer to think in terms of voltages.

### `uartPin`
A `uartPin` is a combination of two real pins for a given `port_id` on the `tx` and `rx` pins. This can be used for a uart interface on a given `uart_id`. 

### `microcontroller`
The microcontroller type is a large struct which contains all of the objects that are relevant for that particular microcontroller.